<div class="footer">
    <div class="row-content-center">
        <div class="footer-content">
            <div class="footer-about-us">
                <div class="footer-header-wget">
                    <h2>JOBSTER</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet provident vel numquam quo dignissimos! Aspernatur est harum veritatis necessitatibus veniam.</p>
                </div>
                <div class="footer-icon-link">
                    <a href="#"><i class="fas fa-facebook-square"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-vk"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
            <div class="footer-quick-links">
                <h6 class="footer-name-wget">Quick Links</h6>
                <ul>
                    <li><a href="home.html">Post New Job</a></li>
                    <li><a href="jobs.html">Jobs List</a></li>
                    <li><a href="job.html">Candidate List</a></li>
                    <li><a href="#">Employer List</a></li>
                    <li><a href="#">Browse Categories</a></li>
                </ul>
            </div>
            <div class="footer-quick-links">
                <h6 class="footer-name-wget">Tranding Jobs</h6>
                <ul>
                    <li><a href="job.html">Designer</a></li>
                    <li><a href="job.html">UI & UX Expert</a></li>
                    <li><a href="job.html">Developer</a></li>
                    <li><a href="job.html">iOS Developer</a></li>
                    <li><a href="job.html">Front-End developer</a></li>
                </ul>
            </div>
            <div class="footer-header-wget">
                <div class="footer-newsletter">
                    <h6 class="footer-name-wget">Newsletter</h6>
                    <p>Subscribe to Lawson to get all latest Job, Resume, Company Listing & Blog post to stay update.</p>
                    <form id="mc-form" class="mc-form" novalidate="true">
                        <input id="mc-email" autocomplete="off" type="email" placeholder="Enter your e-mail address" name="EMAIL">
                        <button id="mc-submit" class="btn"><i class="fa fa-envelope-o"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="basement">
    <p>Copyright  ©  2019 <a href="home.html"> JOBSTER </a>  All Rights Reserved.</p>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jobster - Create Job</title>
    <link rel="icon" href="assets/img/icon-page.png">
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/dekstop.css">
    <link rel="stylesheet" href="assets/css/creation.css">
    <link rel="stylesheet" href="assets/css/mobile.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header-slider" style="background-image: url(assets/img/admin-background.jpg); background-position: 50% 0;;position: relative;">
                <?php include("./templates/menu.php") ?>
                <div class="slider-name-page">
                    <h2>Add a new job</h2>
                    <p>
                        <a href="home.html">Home </a>/  
                        <a href="creation.html">Company </a>/ 
                        Create Job
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-content-center">
        <div class="create-job-form">
            <form action="#">
                <div class="col-job">
                    <label for="jobTitle">Job Title</label>
                    <input type="text" placeholder="e.g. web design">
                </div>
                <div class="col-job">
                    <label for="applicationDeadline">Application Deadline</label>
                    <input type="text" placeholder="12/24/2019">
                </div>
                <div class="col-job">
                    <label for="jobType">Job Type</label>
                    <select id="jobType">
                        <option value="type0">Full Time</option>
                        <option value="type1">Part Time</option>
                        <option value="type2">Freelance</option>
                    </select>
                </div>
                <div class="col-job">
                    <label for="jobCategory">Job Category</label>
                    <select id="jobCategory">
                        <option value="type0">Web Design</option>
                        <option value="type1">Ui Ux Design</option>
                        <option value="type2">WordPress Developer</option>
                        <option value="type3">React Developer</option>
                    </select>
                </div>
                <div class="col-job">
                    <label for="salartRange"> Min.Salary Range</label>
                    <input type="text" placeholder="$3000">
                </div>
                <div class="col-job">
                    <label for="jobGender">Gender</label>
                    <select id="jobGender">
                        <option value="type0">Any</option>
                        <option value="type1">Male</option>
                        <option value="type2">Female</option>
                    </select>
                </div>
                <div class="col-job">
                    <label for="salartRange">Max.Salary Range</label>
                    <input type="text" placeholder="$8000">
                </div>
                <div class="col-job">
                    <label for="jobExperience">Experience</label>
                    <select id="jobExperience">
                        <option value="type0">1 Year to 2 Year</option>
                        <option value="type1">2 Year to 3 Year</option>
                        <option value="type2">3 Year to 4 Year</option>
                        <option value="type3">4 Year to 5 Year</option>
                    </select>
                </div>
                <div class="col-job">
                    <label for="jobEducation">Education</label>
                    <select id="jobEducation">
                        <option value="type0">Postdoc</option>
                        <option value="type1">Ph.D.</option>
                        <option value="type2">Master</option>
                        <option value="type3">Bachelor</option>
                    </select>
                </div>
                <div class="col-job">
                    <label for="jobDescription">Description</label>
                    <textarea id="jobDescription" rows="5"></textarea>
                </div>
                <div class="col-button">
                    <button type="submit" class="btn-primary">Post Job</button>
                </div>
            </form>
        </div>
    </div>
    <?php include("./templates/footer.php") ?>
    <script src="assets/js/scrollUp.js"></script>
</body>
</html>
<div class="row-content-center">
    <div class="header-logo">
        <a href="home.html">
            <h1>JOBSTER</h1>
        </a>
    </div>
    <div class="header-menu">
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="jobs.php">Jobs</a></li>
                <li><a href="job.php">Job Single</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="admin.php">Panel</a></li>
            </ul>
        </nav>
    </div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jobster - Job List</title>
    <link rel="icon" href="assets/img/icon-page.png">
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/dekstop.css">
    <link rel="stylesheet" href="assets/css/about.css">
    <link rel="stylesheet" href="assets/css/mobile.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header-slider" style="background-image: url(assets/img/slider-header2.jpg); background-position: 50% 0;position: relative;">
                <?php include("./templates/menu.php") ?>
                <div class="slider-name-page">
                    <h2>About Us</h2>
                    <p>
                        <a href="home.html">Home </a>/  
                        <a href="about.html">About </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-content-center">
        <div class="about-us">
            <div class="about-info">
                <h2>About JOBSTER</h2>
                <p>It is a private profitable company, established in Albania, and operating in Texas (USA), Greece, Cyprus, Albania, Kosovo, Bosnia and Herzegovina, Macedonia, Serbia and Montenegro.More than 500 clients in these countries have chosen The <strong>JOBSTER</strong> due to the experience of the staff and the founder of the company, when it comes to customise personnel solutions of high competence.The <strong>JOBSTER</strong> is as a strategic partner for companies operating on a national or international footing. We operate in the area of services like: HR consultancy services, employment and training, staff leasing, payroll, mystery shopper service, market research, customer satisfaction evaluation/monitoring and business matching.We have an outstanding network of clients coming from international and local businesses in the region.</p>
                <strong class="about-vision">We build our business by understanding the needs of our customers and finding new and better ways to meet those needs.</strong>
            </div>
            <img src="assets/img/about-1.jpg" alt="About Image">
        </div>
        <div class="about-team">
            <h2>Our Team</h2>
            <p>Our team are build by all professional members who <br> always try to get the efficiency work.</p>
        </div>
        <div class="about-team-image">
            <div class="team-img">
                <img src="assets/img/team/team-1.jpg" alt="">
                <div class="team-content">
                    <div class="team-inner">
                        <h6>Matthew Wilder</h6>
                        <p>Founder</p>
                    </div>
                </div>
            </div>
            <div class="team-img">
                <img src="assets/img/team/team-2.jpg" alt="">
                <div class="team-content">
                    <div class="team-inner">
                        <h6>Jameson Bau</h6>
                        <p>Project Manager</p>
                    </div>
                </div>
            </div>
            <div class="team-img">
                <img src="assets/img/team/team-3.jpg" alt="">
                <div class="team-content">
                    <div class="team-inner">
                        <h6>Alba Simon</h6>
                        <p>Designer</p>
                    </div>
                </div>
            </div>
            <div class="team-img">
                <img src="assets/img/team/team-4.jpg" alt="">
                <div class="team-content">
                    <div class="team-inner">
                        <h6>Chris Walker</h6>
                        <p>Backend Developer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include("./templates/menu.php") ?>
    <script src="assets/js/scrollUp.js"></script>
</body>
</html>
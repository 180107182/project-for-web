<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jobster - Job List</title>
    <link rel="icon" href="assets/img/icon-page.png">
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/dekstop.css">
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/mobile.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header-slider" style="background-image: url(assets/img/admin-background.jpg); background-position: 50% ; height: 100px;position: relative;">
                <?php include("./templates/menu.php") ?>
            </div>
        </div>
    </div>
    <div class="row-content-center">
        <table class="table-jobs">
            <h2 class="list-vacancies">List Vacancies</h2>
            <a class="creat-job" href="creation.php">+</a>
            <tr class="table-list-header">
                <th>Date</th>
                <th>Name Vacancy</th>
                <th>Name Company</th>
                <th>Edit Vacancies</th>
                <th>Delete Vavancies</th>
            </tr>
            <?php
                include "./php/db.php";

                $sql = "SELECT * FROM vacancies";

                $result = $conn->query($sql);
                
                if (isset($_GET['del'])) {
                    $id = $_GET['del'];

                    $query = "DELETE FROM vacancies WHERE id=$id";
                    mysqli_query($conn, $query) or die( mysqli_error($conn));
                }

                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '
                            <tr><td>' . $row["date"] . '</td><td>' . $row["vacancyName"] . '</td><td>' . $row["companyName"] . '</td><td><a href="#">Edit</a></td><td><a href="?del=' . $row["id"] . '">Delete</a></td></tr>
                        ';
                    }
                }
                
                $conn->close();
            ?>
        </table>
    </div>
    
    



    <?php include("./templates/footer.php") ?>
    <script src="assets/js/scrollUp.js"></script>
</body>
</html>
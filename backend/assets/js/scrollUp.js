var b = document.body || document.getElementsByTagName('body')[0];
b.insertAdjacentHTML('beforeend', '<button onclick="topFunction()" id="toTop" title="Go to top"><img src="assets/img/icon/scroll-up.png" alt=""></button>');

document.getElementById("toTop").setAttribute("style", "display: none; position: fixed; bottom: 18px; right: 18px; z-index: 1000; border: none; outline: none; background: none; cursor: pointer;");

document.documentElement.setAttribute("style", "scroll-behavior: smooth;");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
	let t = document.getElementById("toTop");
  	if (document.body.scrollTop > 280 || document.documentElement.scrollTop > 480) {
  		t.style.display = "block";
  	} else {
  		t.style.display = "none";
  	}
}
function topFunction() {
  	document.body.scrollTop = 0; // For Safari
  	document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jobster - Jobs</title>
    <link rel="icon" href="assets/img/icon-page.png">
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/dekstop.css">
    <link rel="stylesheet" href="assets/css/jobs.css">
    <link rel="stylesheet" href="assets/css/mobile.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header-slider" style="background-image: url(assets/img/slider-header2.jpg); background-position: 50% 0;position: relative;">
                <?php include("./templates/menu.php") ?>
                <div class="slider-name-page">
                    <h2>Browse Jobs</h2>
                    <p><a href="home.html">Home </a>/ Jobs</p>
                </div>
            </div>
        </div>
    </div>
    <div class="job-list-toolbar">
        <div class="row-content-center">
            <p>Showing 1 - 10 of 34 results</p>
            <p class="title-toolbar">
                Sort by : 
                <select>
                    <option>Most Recent</option>
                    <option>Top Rated</option>
                    <option>Most Popular</option>
                </select>
            </p>
        </div>
    </div>
    <div class="row-content-center">
        <div class="content-center">
            <?php 
                include "./php/db.php";

                $sql = "SELECT * FROM vacancies";
                
                if (isset($_POST['enter'])) {
                    $sql = "SELECT * FROM vacancies WHERE upper(vacancyName) LIKE '%" . strtoupper($_POST["search-input"]) . "%'";
                }

                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '
                            <div class="job-list-wrap">
                                <a class="job-list" href="job.php?id=' . $row["id"] . '">
                                    <div class="company-logo">
                                        <img src="assets/img/companies/company-1.png" alt="compant logo">
                                    </div>
                                    <div class="company-col">
                                        <h6>' . $row["vacancyName"] . '</h6>
                                        <span>' . $row["companyName"] . '</span>
                                    <p><i class="fa fa-map-marker"></i>' . $row["address"] . '</p>
                                    </div>
                                    <div class="company-salary">
                                        <p>' . $row["minSalary"] . ' - ' . $row["maxSalary"] . '</p>
                                        <span class="salary-span1">' . $row["employmentStatus"] . '</span>
                                    </div>
                                </a>
                            </div>
                        ';
                    }
                }

                $conn->close();
            ?>
            <ul class="pagnitation">
                    <li><a href="#"></a><</li>
                    <li><a href="#"></a>1</li>
                    <li><a href="#"></a>2</li>
                    <li><a href="#"></a>3</li>
                    <li><a href="#"></a>4</li>
                    <li><a href="#"></a>></li>
                </ul>
        </div>  

            
        <div class="job-details">
            <div class="inner">
                <h6>Search Keywords</h6>
                <div class="inner-input">
                    <input type="text" placeholder="y.g. web design">
                    <a href="#"></a>
                </div>
            </div>
            <div class="inner">
                <h6>Category</h6>
                <select>
                    <option value="1">Any Category</option>
                    <option value="2">Web Designer</option>
                    <option value="3">Web Developer</option>
                    <option value="4">Graphic Designer</option>
                    <option value="5">App Developer</option>
                    <option value="6">UI & UX Expert</option>
                </select>
            </div>
            <div class="inner">
                <h6>Location</h6>
                <div class="inner-input">
                    <input type="text" placeholder="Location">
                    <a href="#"></a>
                </div>
            </div>
            <div class="inner">
                <h6>Job Type</h6>
                <form action="#" class="mb1">
                    <div class="custom-control">
                        <input type="checkbox" class="custom-control-input" id="jobtype0">
                        <label class="custom-control-label" for="jobtype0">All Type</label>
                    </div>
                    <div class="custom-control">
                        <input type="checkbox" class="custom-control-input" id="jobtype1">
                        <label class="custom-control-label" for="jobtype1">Full Time</label>
                    </div>
                    <div class="custom-control">
                        <input type="checkbox" class="custom-control-input" id="jobtype2">
                        <label class="custom-control-label" for="jobtype2" class="salary-span2">Part Time</label>
                    </div>
                    <div class="custom-control">
                        <input type="checkbox" class="custom-control-input" id="jobtype3">
                        <label class="custom-control-label" for="jobtype3" class="salary-span3">Freelance</label>
                    </div>
                    <div class="custom-control">
                        <input type="checkbox" class="custom-control-input" id="jobtype4">
                        <label class="custom-control-label" for="jobtype4">Internship</label>
                    </div>
                    <div class="custom-control">
                            <input type="checkbox" class="custom-control-input" id="jobtype5">
                            <label class="custom-control-label" for="jobtype5">Temporary</label>
                    </div>
                </form>
            </div>
            <div class="inner">
                <h6>Experince</h6>
                <form action="#" class="mb1">
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype0">
                        <label class="custom-control-label" for="jobtype0">Any Experince</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype1">
                        <label class="custom-control-label" for="jobtype1">1 Year to 2 Year</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype2">
                        <label class="custom-control-label" for="jobtype2">2 year to 3 Year</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype3">
                        <label class="custom-control-label" for="jobtype3">3 Year to 4 Year</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype4">
                        <label class="custom-control-label" for="jobtype4">4 Year to 5 Year</label>
                    </div>
                </form>
            </div>
            <div class="inner">
                <h6>Date Posted</h6>
                <form action="#" class="mb1">
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype0">
                        <label class="custom-control-label" for="jobtype0">Any Date</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype1">
                        <label class="custom-control-label" for="jobtype1">Last Hour</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype2">
                        <label class="custom-control-label" for="jobtype2">Last 24 Hours</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype3">
                        <label class="custom-control-label" for="jobtype3">Last 7 Days</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype4">
                        <label class="custom-control-label" for="jobtype4">Last 14 Days</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype5">
                        <label class="custom-control-label" for="jobtype5">Last 30 Days</label>
                    </div>
                </form>
            </div>
            <div class="inner">
                <h6>Job Type</h6>
                <form action="#" class="mb1">
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype0">
                        <label class="custom-control-label" for="jobtype0">Matriculation</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype1">
                        <label class="custom-control-label" for="jobtype1">Intermidiate</label>
                    </div>
                    <div class="custom-control">
                        <input type="radio" class="custom-control-input" id="jobtype2">
                        <label class="custom-control-label" for="jobtype2">Gradute</label>
                    </div>
                </form>
            </div>
        </div>    
    </div>
    <?php include("./templates/footer.php") ?>
    <script src="assets/js/scrollUp.js"></script>
</body>
</html>

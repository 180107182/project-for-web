<!DOCTYPE html>
<html lang="en">
<head>
    <title>Jobster - Home</title>
    
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <!-- Icon -->
    <link rel="icon" href="assets/img/icon-page.png">
    
    <!-- Inner CSS -->
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/dekstop.css">
    <link rel="stylesheet" href="assets/css/home.css">
    <link rel="stylesheet" href="assets/css/mobile.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Outer CSS-->
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header-slider" style="background-image: url(assets/img/slider-header.jpg); background-position: 50%;position: relative;">
                <?php include("./templates/menu.php") ?>
                <div class="content-search">
                    <div class="search-header">
                        <h2>FIND YOUR NEXT JOB</h2>
                        <p>More then 1,524 job listed here.</p>
                    </div>
                    <form action="jobs.php" method="POST" class="search-form">
                        <input class="header-search-form" name="search-input" type="search" id="sEarch" placeholder="Vacancy name"><a href=""></a></input>
                        <input class="search-button" name="enter" type="submit" value="search" id="Search">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--List jobs -->
    <div class="row-content-center">
        <div class="latest-jobs-header">
            <h2>Latest Jobs</h2>
            <p>Here's the most recent job listed on the website.</p>
        </div>
        <?php 
            include "./php/db.php";

            $sql = "SELECT * FROM vacancies";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo '
                        <div class="job-list-wrap">
                            <a class="job-list" href="job.php?id=' . $row["id"] . '">
                                <div class="company-logo">
                                    <img src="assets/img/companies/company-1.png" alt="compant logo">
                                </div>
                                <div class="company-col">
                                    <h6>' . $row["vacancyName"] . '</h6>
                                    <span>' . $row["companyName"] . '</span>
                                    <p><i class="fa fa-map-marker"></i>' . $row["address"] . '</p>
                                </div>
                                <div class="company-salary">
                                    <p>' . $row["minSalary"] . ' - ' . $row["maxSalary"] . '</p>
                                    <span class="salary-span1">' . $row["employmentStatus"] . '</span>
                                </div>
                            </a>
                        </div>
                    ';
                }
            }

            $conn->close();
        ?>
        <div class="company-btn">
            <p><a href="jobs.php">View ALL Jobs</a></p>
        </div>
    </div>
    <?php include("./templates/footer.php") ?>
    <script src="assets/js/scrollUp.js"></script>
    <script src="liveSearch.js"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jobster - Job List</title>
    <link rel="icon" href="assets/img/icon-page.png">
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/dekstop.css">
    <link rel="stylesheet" href="assets/css/job.css">
    <link rel="stylesheet" href="assets/css/jobs.css">
    <link rel="stylesheet" href="assets/css/mobile.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header-slider" style="background-image: url(assets/img/slider-header2.jpg); background-position: 50% 0;position: relative;">
                <?php include("./templates/menu.php") ?>
                <div class="slider-name-page">
                    <h2>Full Stack Backend Developer</h2>
                    <p>
                        <a href="home.html">Home </a>/  
                        <a href="jobs.html">Jobs </a>/ 
                        Full Stack Backed Developer
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-content-center">
        <div class="content-company-info">
            <div class="job-list-wrap">
                <div class="job-logo">
                    <img src="assets/img/companies/company-1.png" alt="compant logo">
                </div>
                <div class="job-col">
                    <h6>Full Stack Backend Developer</h6>
                    <span>Envato</span>
                    <p><i class="fa fa-map-marker"></i>2020 Willshire Glen,GA-30009</p>
                </div>
                <div class="company-salary">
                    <p>$5000-$8000</p>
                    <span class="salary-span1">Full Time</span>
                </div>
            </div>
            <div class="job-details-sidebar">
                <div class="job-details">
                    <h6 class="job-details-name">Job Description</h6>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Similique, ex iusto! Tenetur iusto dolore amet voluptates esse? Ut debitis perferendis, impedit ullam ea officia sapiente soluta cupiditate molestiae eius enim aut laboriosam, saepe deleniti. Excepturi nobis amet fugit ipsa corrupti!</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo ratione odit qui inventore maiores labore tenetur earum! Quam eaque, deleniti quibusdam deserunt quos reprehenderit dolor, in quo voluptates maxime nostrum.</p>
                    <h6 class="job-details-name">Responsibilities</h6>
                    <ul>
                        <li>• Proven work experienceas a web designer</li>
                        <li>• Demonstrable graphic design skills with a strong portfolio</li>
                        <li>• Proficiency in HTML, CSS and JavaScript for rapid prototyping</li>
                        <li>• Experience working in an Agile/Scrum development process</li>
                        <li>• Proven work experienceas a web designer</li>
                        <li>• Excellent visual design skills with sensitivity to user-system interaction</li>
                        <li>• Ability to solve problems creatively and effectively</li>
                        <li>• Proven work experienceas a web designer</li>
                        <li>• Up-to-date with the latest Web trends, techniques and technologies</li>
                        <li>• BS/MS in Human-Computer Interaction, Interaction Design or a Visual Arts subject</li>
                    </ul>
                    <h6 class="job-details-name">Education + Experience</h6>
                    <ul>
                        <li>• Advanced degree or equivalent experience in graphic and web design</li>
                        <li>• 3 or more years of professional design experience</li>
                        <li>• Direct response email experience</li>
                        <li>• Ecommerce website design experience</li>
                        <li>• Familiarity with mobile and web apps preferred</li>
                        <li>• Excellent communication skills, most notably a demonstrated ability to solicit and address creative and design feedback</li>
                        <li>• Must be able to work under pressure and meet deadlines while maintaining a positive attitude and providing exemplary customer service</li>
                        <li>• Ability to work independently and to carry out assignments to completion within parameters of instructions given, prescribed routines, and standard accepted practices</li>
                    </ul>
                    <h6 class="job-details-name">Benefits</h6>
                    <ul>
                        <li>• Medical insurance</li>
                        <li>• Dental insurance</li>
                        <li>• Vision insurance</li>
                        <li>• Supplemental benefits (Short Term Disability, Cancer & Accident).</li>
                        <li>• Employer-sponsored Basic Life & AD&D Insurance</li>
                        <li>• Employer-sponsored Long Term Disability</li>
                        <li>• Employer-sponsored Value Adds – Fresh Beanies</li>
                        <li>• 401(k)with matching</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="job-sidebar">
            <div class="inner">
                <div class="sidebar-btn">
                    <a class="btn-sidebar-1" href="#"><i class="fa fa-heart-o"></i>Save Job</a>
                    <a class="btn-sidebar-2" href="#">Apply Now</a>
                </div>
            </div>
            <div class="inner">
                <h6>Job Overview</h6>
                <ul>
                    <li><strong>Published on: </strong>Nov 6, 2019</li>
                    <li><strong>Vacancy: </strong>02</li>
                    <li><strong>Employment Status: </strong>Full-time</li>
                    <li><strong>Experience: </strong>2 to 3 year(s)</li>
                    <li><strong>Job Location: </strong>Willshire Glen</li>
                    <li><strong>Salary: </strong>$5000-$8000</li>
                    <li><strong>Gender: </strong>Any</li>
                    <li><strong>Application Deadline: </strong>Dec 15, 2019</li>
                </ul>
            </div>
            <div class="inner">
                <h6>Job Location</h6>
                <div class="location">
                    <iframe width="100%" height="440" src="https://maps.google.com/maps?width=550&amp;height=440&amp;hl=en&amp;q=melburn+(Keller)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><br /></iframe>
                </div>
            </div>
        </div>
    </div>
    <?php include("./templates/footer.php") ?>
    <script src="assets/js/scrollUp.js"></script>
</body>
</html>